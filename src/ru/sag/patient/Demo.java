package ru.sag.patient;

public class Demo {
    public static void main(String[] args) {
        Patient andrey = new Patient("Андрей ", 2003, 4, false);
        Patient denis = new Patient("Денис ", 2001, 7, false);
        Patient nikita = new Patient("Никита ", 2002, 9, false);
        Patient emil = new Patient("Эмиль ", 2003, 3, false);
        Patient alexey = new Patient("Алексей ", 1998, 1, true);

        Patient[] patients = {andrey, denis, nikita, emil, alexey};

        witchThe2000Birthday(patients);
    }

    private static void witchThe2000Birthday(Patient[] patients) {

        for (Patient patient :
                patients) {
            if (patient.isDispensary() && patient.getBirthday() < 2000) {

                System.out.println("Пациенты рожденные раньше 2000 г. " + patients);
            }
        }

    }
}
