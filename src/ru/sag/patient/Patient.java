package ru.sag.patient;

public class Patient {
    private String name;
    private int birthday;
    private int number;
    private boolean dispensary;

    Patient(String name, int birthday, int number, boolean dispensary) {
        this.name = name;
        this.birthday = birthday;
        this.number = number;
        this.dispensary = dispensary;
    }

    Patient() {
        this("Не указана", 0, 0, false);
    }

    public String getName() {
        return name;
    }

    public int getBirthday() {
        return birthday;
    }

    public int getNumber() {
        return number;
    }

    public boolean isDispensary() {
        return dispensary;
    }

    @Override
    public String toString() {
        return "Пациент {" +
                "Имя = " + name +
                ", Год рождения = " + birthday +
                ", Номер карточки = " + number +
                ", Диспансеризация = " + dispensary +
                '}';
    }
}
